//创建连接池
//导入mysql模块
const mysql = require('mysql');
//创建连接池
const pool = mysql.createPool({
  host:'127.0.0.1',
  port:"3306",
  user:"root",
  password:'',
  database:'your_stylist',
  connectionLimit:15 //连接池大小，默认为15
});
module.exports=pool;