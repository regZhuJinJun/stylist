#设置客户端连接服务器编码为utf8
SET  names utf8; 
#显示数据
show databases;
#删除数据库如果存在
DROP DATABASE IF EXISTS your_stylist;
#创建数据库
CREATE DATABASE your_stylist charset=utf8;
#进入数据库
USE  your_stylist;
#创建管理员数据库
CREATE TABLE admin(
  a_id INT auto_increment primary key,
  a_names varchar(8) not null unique,
  a_pwd varchar(255) not null
);
INSERT INTO
  admin(a_names, a_pwd)
VALUES
  ('zhangsan', '123'),
  ('lisi', '123'),
  ('wangwu', '123');
#创建数据用户表
CREATE TABLE users(
  u_id INT auto_increment primary key,
  u_name VARCHAR(8) not null unique,
  u_pwd VARCHAR(16) NOT null,
  u_sex VARCHAR(1),#1-男  0-女
  u_age INT,
  u_habby VARCHAR(128),
  u_wish  VARCHAR(128),
  u_phone  VARCHAR(11),
  u_address VARCHAR(128)
);
#创建造型师数据表
CREATE TABLE stylists(
  s_id INT auto_increment primary key,
  s_name VARCHAR(8) not null unique,
  s_sex VARCHAR(1), #1-男  0-女
  s_skills VARCHAR(256),
  s_product VARCHAR(256)
);
#创建商品数据表
CREATE TABLE products(
  product_name VARCHAR(56) not null,
  price varchar(8) not null,
  Brand VARCHAR(128) not null,  #品牌
  Material  VARCHAR(128) ,  #材质
  Pattern VARCHAR(128),  #图案
  style VARCHAR(128),   #风格
  color VARCHAR(8),   #颜色
  time_market VARCHAR(8) ,  #上架时间
  No VARCHAR(128),  #货号
  Applicable_season VARCHAR(2), #使用季节
  clothing_style_details VARCHAR(1),  # 适用对象 1-男 0-女
  Applicable_object VARCHAR(256), #产品图
  Shipping_address VARCHAR(128)  #发货地址
);
#创建子数据库---AI测脸数据库
#CREATE DATABASE AI;

