//导入express框架
const express = require('express');
//创建路由器(路由的集合)
const r = express.Router();
//调用连接池模块
r.use('../pool.js')
//添加路由
//设置用户接口地址
app.post('/user',(req,res)=>{
  res.send({code:200,msg:'注册成功'});
});
//设置造型师接口地址
app.post('/stylist',(req,res)=>{
  res.send({code:200,msg:'注册成功'});
});
//设置商品接口地址
app.post('/product',(req,res)=>{
  res.send({code:200,msg:'添加成功'});
});
//商家注册接口地址
app.post('/business',(req,res)=>{
  res.send({code:200,msg:'注册成功'});
});

//导出路由器
module.exports=r;