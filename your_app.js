//引入express框架
const express = require('express');
//导入Login路由器
const LoginRouter = require('./router/admin.js')
//创建web服务器
const app = express();
//设置端口号
app.listen(8080);
//设置静态托管
app.use(express.static('./views'));
//设置用户接口地址
app.use(express.urlencoded({extended:false}))
app.use('/admin',LoginRouter);
//使用错误处理中间件，拦截所有路由抛出的错误
app.use((err,req,res,next)=>{  
	                                          
	//err得到的中间件传递过来的错误
	console.log(err);
	//设置http协议响应的状态
	res.status(500).send({code:500,msg:'服务器端错误',});
});
//